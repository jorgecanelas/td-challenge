#Create VPC
resource "aws_vpc" "challenge" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  instance_tenancy = "default"

  tags = {
    Name = "Challenge VPC"
  }
}

#Create subnet in Ireland eu-west-1a
resource "aws_subnet" "challenge-sub-a" {
  cidr_block = "10.0.0.0/24"
  vpc_id = aws_vpc.challenge.id
  availability_zone = "eu-west-1a"

  tags = {
    Name = "Challenge subnet A"
  }
}

#Create subnet in Ireland eu-west-1b
resource "aws_subnet" "challenge-sub-b" {
  cidr_block = "10.0.0.0/24"
  vpc_id = aws_vpc.challenge.id
  availability_zone = "eu-west-1b"

  tags = {
    Name = "Challenge subnet B"
  }
}

#Create Internet Gateway to allow traffic Between internet and VPC
resource "aws_internet_gateway" "challenge" {
  vpc_id = aws_vpc.challenge.id

  tags = {
    Name = 'Challenge Internet Gateway'
  }
}

#Create Security Group to allow traffic on port 22 and 80
resource "aws_security_group" "challenge-nat" {
  name = "Challenge NAT"
  vpc_id = aws_vpc.challenge.id

  #http port for Supermario container
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  #ssh port for ansible
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

#Get Amazon Linux 2 AMI
data "aws_ami" "amazon_linux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }

  owners = ["amazon"]
}

#Create EC2 supermario1 to run docker container with supermario image
resource "aws_instance" "supermario1" {
  ami = data.aws_ami.amazon_linux.id
  instance_type = "t2.small"
  availability_zone = "eu-west-1a"
  monitoring = true
  key_name = "jorge.canelas"
  subnet_id = aws_subnet.challenge-sub-a.id
  vpc_security_group_ids = [
    aws_security_group.challenge-nat.id
  ]
  disable_api_termination = true

  #Config ec2 disk with 20G, 8G is more enough but logs consume space
  root_block_device {
    volume_type = "gp2"
    volume_size = 20
    delete_on_termination = false
  }

  tags = {
    Name = "Super Mario 1"
  }
}

#Create EC2 supermario2 to run docker container with supermario image
resource "aws_instance" "supermario2" {
  ami = data.aws_ami.amazon_linux.id
  instance_type = "t2.small"
  availability_zone = "eu-west-1b"
  monitoring = true
  key_name = "jorge.canelas"
  subnet_id = aws_subnet.challenge-sub-b.id
  vpc_security_group_ids = [
    aws_security_group.challenge-nat.id
  ]
  disable_api_termination = true

  #Config ec2 disk with 20G, 8G is more enough but logs consume space
  root_block_device {
    volume_type = "gp2"
    volume_size = 20
    delete_on_termination = false
  }

  tags = {
    Name = "Super Mario 2"
  }
}

#Create Elastic Load Balancer to forward traffic on port 80 to EC2 port 80
resource "aws_elb" "challenge-elb" {
  name = "Challenge ELB"
  availability_zones = ["eu-west-1a", "eu-west-1b"]
  subnets = [aws_subnet.challenge-sub-a, aws_subnet.challenge-sub-b]
  security_groups = [aws_security_group.challenge-nat]

  #Forward port 80 to 80
  listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }

  #Check if supermario container's are alive
  health_check {
    healthy_threshold = 3
    interval = 30
    target = "HTTP:80/"
    timeout = 3
    unhealthy_threshold = 3
  }

  instances = [aws_instance.supermario1, aws_instance.supermario2]

  tags = {
    Name = 'Challenge Elastic Load Balancer'
  }
}
